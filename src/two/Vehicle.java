package two;

public abstract class Vehicle {
	private String num;

	public Vehicle(String num){
		this.num = num;
	}
	
	public abstract int move();
	
	public String toString(){
		return this.num;
	}

}
